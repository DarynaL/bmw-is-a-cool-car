BMW is a cool car

The project consists of three classes where the superclass is named Vehicle,its inheritance class is named Cars, and the last class BMW 5 series sedan extends class Cars. The superclass describes all common vehicle features and methods, the inheritance classes extend its methods and features specific for Cars and BMW 5 series sedan resp.

Methods of Cars class:

- hasEngine() - checks whether car has engine or not(depends on motorized field)
- info() - prints to console all info about vehicle 
- guessVehicleType() - guesses type of vehicle(depends on wheels field)

Method of Cars class:

- isStolen(): checks whether car is stolen or not(depends on vinCode field)

Method of BMW 5 series sedan class:

- getAcceleration(): guesses the acceleration time of a car(depends on engineVol field)

In each inheritance class method info() is overrided. 

UML:

![uml_2__1_](/uploads/7efa2f8b4d36eddf8e6354a9b60af571/uml_2__1_.png)


